package gUgorski.app;

import gUgorski.io.FileManager;
import gUgorski.io.OptionDoestExist;

import java.io.FileNotFoundException;
import java.util.InputMismatchException;
import java.util.Scanner;

class AppControl {

    private Scanner scanner = new Scanner(System.in);
    private SudokuManager sudokuManager = new SudokuManager();

    private void resolve() {
        boolean finish = false;
        while (!finish) {
            System.out.println("Podaj cyfrę, literę i cyfrę (kolumna, wiersz, wartość do wprowadzenia) lub " +
                    "\"e\" aby sprawdzić rozwiązanie lub \"b\" aby cofnąć.");
            String input = scanner.next();

            if (input.matches("^[i]$")) {
                sudokuManager.selfResolveInSquareWhenOneCellIsEmpty();
                continue;
            }

            if (input.matches("^[b]$")) {
                sudokuManager.moveBack();
                continue;
            }

            if (input.matches("^[h]$")) {
                sudokuManager.sugestionInRows();
                sudokuManager.sugestionInColumns();
                sudokuManager.sugestionInSquare();
                continue;
            }
            if (input.matches("^[e]$")) {
                sudokuManager.isPrepared = false;
                finish = true;
                sudokuManager.printAfterResolve();
                if (sudokuManager.isResolve()) {
                    System.out.println("Rozwiązanie poprawne.");
                } else System.out.println("Rozwiązanie niepoprawne.");
                System.out.println();
                continue;
            }
            if (!input.matches("^[1-9][a-i][1-9]$")) {

                System.err.println("Zły format wprowadzonych danych.");
                continue;
            }

            int column = Integer.valueOf(input.substring(0, 1));
            int row = input.charAt(1) - 96;
            int value = Integer.valueOf(input.substring(2, 3));

            if (sudokuManager.isValueExist(column, row)) {
                System.err.println("Wartość w tym miejscu istnieje.");
            } else {
                sudokuManager.resolvingSudoku(column, row, value);
                sudokuManager.printWithoutTime();
            }
        }
    }

    private void prepareToResolve() {
        sudokuManager.prepareToResolve();
//        sudokuManager.printWithoutTime();
    }

    private void readGridFromFile() throws FileNotFoundException {
        long startTime = System.currentTimeMillis();

        FileManager fileManager = new FileManager();
        sudokuManager.readGridFromFile(fileManager.readGridFromFile());
        sudokuManager.sudokuCreateTime = (System.currentTimeMillis() - startTime) / 1000f;
        sudokuManager.printWithTime();


    }

    private void createSudokuAllGrid() {
        sudokuManager.createSudokuAllGrid();
        FileManager fileManager = new FileManager();
        sudokuManager.printWithTime();
        fileManager.saveGridToFile(sudokuManager.sudoku);
    }

    void controlLoop() throws FileNotFoundException {
        Option option;
        do {
            printOptions();
            option = getOption();
            switch (option) {
                case GENERATE_SUDOKU:
                    createSudokuAllGrid();
                    break;
                case READ_SUDOKU_FROM_FILE:
                    readGridFromFile();
                    break;
                case RESOLVE_SUDOKU:
                    prepareToResolve();
                    if (sudokuManager.isPrepared)
                        resolve();
                    break;
                case EXIT:
                    exit();
                    break;
                default:
                    System.out.println("Niedozwolony znak!");
            }
        } while (option != Option.EXIT);
    }

    private Option getOption() {
        boolean optionOk = false;
        Option option = null;
        while (!optionOk) {
            try {
                option = Option.createFromInt(scanner.nextInt());
                optionOk = true;
            } catch (OptionDoestExist e) {
                System.err.println(e.getMessage() + ", wprowadź ponownie:");
            } catch (InputMismatchException i) {
                System.err.println("Nie wprowadzono liczby, spróbuj jeszcze raz:");
                scanner.nextLine();
            }
        }
        return option;
    }

    private void exit() {
        scanner.close();
        System.out.println("Koniec programu.");
    }

    private void printOptions() {
        System.out.println("Wybierz opcję: ");
        for (Option option : Option.values()) {
            System.out.println(option.toString());
        }
    }

    private enum Option {
        EXIT(0, "Wyjście z programu"),
        GENERATE_SUDOKU(1, "Generuj i zapisz do pliku Sudoku"),
        READ_SUDOKU_FROM_FILE(2, "Wczytaj z pliku Sudoku"),
        RESOLVE_SUDOKU(3, "Przeygotuj i rozwiąż Sudoku");

        private int value;
        private String description;

        Option(int value, String description) {
            this.value = value;
            this.description = description;
        }

        static Option createFromInt(int option) throws OptionDoestExist {
            try {
                return Option.values()[option];
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new OptionDoestExist("Brak opcji: " + option);
            }
        }

        @Override
        public String toString() {
            return value + " - " + description;
        }
    }
}
