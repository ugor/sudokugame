package gUgorski.app;

import java.util.*;

class SudokuManager {
    private static final int NUMBER_OF_CELLS = 9;

    float sudokuCreateTime;

    boolean isPrepared = false;

    private Random random = new Random();
    private int countingDrawnNumbers = 0;
    private boolean duplicateInSquare = false;
    private int[][] sudokuGrid = new int[NUMBER_OF_CELLS][NUMBER_OF_CELLS];
    private int[][] selectedGrid = new int[NUMBER_OF_CELLS][NUMBER_OF_CELLS];

    String sudoku = "";

    LinkedList<Integer> stack = new LinkedList<>();

    void selfResolveInSquareWhenOneCellIsEmpty() {
        int y = 0;
        TreeSet<Integer> treeSet = new TreeSet<>();
        int numberNotInSquare = 0;
        for (int i = 0; i < NUMBER_OF_CELLS; i++) {
            treeSet.clear();
            int counterZeroInSquare = 0;
            if (i == 3 || i == 6) {
                y = 0;
            }
            for (int j = 0; j < NUMBER_OF_CELLS; j++, y++) {
                if (j == 3 || j == 6) {
                    y -= 3;
                }
                int x = 3 * (i / 3) + (j / 3);
                treeSet.add(sudokuGrid[x][y]);
                if (sudokuGrid[x][y] == 0) {
                    counterZeroInSquare++;
                }
            }
            if (counterZeroInSquare == 1) {
                for (int t = 1; t <= 9; t++) {
                    if (!treeSet.contains(t)) {
                        numberNotInSquare = t;
                    }

                }
                System.out.println("W kwadracie nr " + (i + 1) + " jest jedno pole wolne i można tam umieścić cyfrę: " + numberNotInSquare);
                System.out.println();
            }
        }
    }

    String iteratorForAllSquare() {
        int y = 0;
        String str = "";
        for (int i = 0; i < NUMBER_OF_CELLS; i++) {
            if (i == 3 || i == 6) {
                y = 0;
            }
            for (int j = 0; j < NUMBER_OF_CELLS; j++, y++) {
                if (j == 3 || j == 6) {
                    y -= 3;
                }
                int x = 3 * (i / 3) + (j / 3);
                str = str + x + y;
                //System.out.println(x+" "+y);
            }
        }
        return str;
    }

    void sugestionInRows() {
        for (int i = 1; i <= NUMBER_OF_CELLS; i++) {
            int row = 1;
            System.out.print(i + " moze byc w wierszu: ");
            for (int x = 0; x < NUMBER_OF_CELLS; x++) {
                boolean inThisRow = false;
                for (int y = 0; y < NUMBER_OF_CELLS; y++) {
                    if (sudokuGrid[x][y] == i) {
                        inThisRow = true;
                    }
                }
                if (!inThisRow) {
                    System.out.print(row + ", ");
                }
                row++;
            }
            System.out.println();

        }
        System.out.println();
    }

    void sugestionInColumns() {
        for (int i = 1; i <= NUMBER_OF_CELLS; i++) {
            int column = 1;
            System.out.print(i + " moze byc w kolumnie: ");
            for (int y = 0; y < NUMBER_OF_CELLS; y++) {
                boolean inThisColumn = false;
                for (int x = 0; x < NUMBER_OF_CELLS; x++) {
                    if (sudokuGrid[x][y] == i) {
                        inThisColumn = true;
                    }
                }
                if (!inThisColumn) {
                    System.out.print(column + ", ");
                }
                column++;
            }
            System.out.println();
        }
        System.out.println();
    }

    void sugestionInSquare() {
        for (int y = 1; y <= NUMBER_OF_CELLS; y++) {
            int x = 0;
            boolean isExist = false;
            String squareNr = "";
            System.out.print(y + " może być w kwadracie: ");
            for (int i = 0; i < 9; i++) {
                isExist = false;
                if (i == 3 || i == 6) {
                    x = 0;
                }
                for (int j = 0; j < 9; j++, x++) {
                    if (j == 3 || j == 6) {
                        x -= 3;
                    }
                    if (sudokuGrid[3 * (i / 3) + (j / 3)][x] == y) {
                        isExist = true;
                    }
                    //System.out.print(3 * (i / 3) + (j / 3) +" "+x);
                }
                if (!isExist) {

                    squareNr += (i + 1) + ", ";
                }
            }
            System.out.print(squareNr);

            System.out.println();

        }
        System.out.println();
    }

    void moveBack() {
        if (stack.size() != 0) {
            int column = stack.getLast();
            stack.removeLast();
            int row = stack.getLast();
            stack.removeLast();

            sudokuGrid[column][row] = 0;
            printWithoutTime();
        } else System.err.println("Brak ruchów wstecz.");


    }

    private void pushOnStack(int column, int row) {
        stack.add(row);
        stack.add(column);
    }

    boolean isValueExist(int row, int column) {
        return sudokuGrid[column - 1][row - 1] != 0;
    }

    boolean isResolve() {
        for (int i = 0; i < NUMBER_OF_CELLS; i++) {
            for (int j = 0; j < NUMBER_OF_CELLS; j++) {
                if (sudokuGrid[i][j] != selectedGrid[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    private void copy(int[][] tab1, int[][] tab2) {
        for (int i = 0; i < NUMBER_OF_CELLS; i++) {
            for (int j = 0; j < NUMBER_OF_CELLS; j++) {
                tab1[i][j] = tab2[i][j];
            }
        }
    }

    private void printGrid(int[][] tab, boolean Showime) {
        StringBuilder sb = new StringBuilder();
        if (Showime) {
            sb.append("    1.2.3.4.5.6.7.8.9.\n    - - - - - - - - - Crated/loaded in " + sudokuCreateTime + " second\n");
        } else sb.append("    1.2.3.4.5.6.7.8.9.\n    - - - - - - - - - \n");

        char charForNavi = 'a';
        for (int i = 0; i < NUMBER_OF_CELLS; i++) {
            sb.append(charForNavi + " | ");
            for (int j = 0; j < NUMBER_OF_CELLS; j++) {
                if (tab[i][j] != 0)
                    sb.append(tab[i][j] + " ");
                else sb.append("  ");

            }
            sb.append("\n");
            charForNavi++;
        }
        //sb.append("\n");
        sudoku = sb.toString() + "\n";
        System.out.println(sb);
    }

    void printWithTime() {
        printGrid(sudokuGrid, true);
    }

    void printWithoutTime() {
        printGrid(sudokuGrid, false);

    }

    void printAfterResolve() {
        String gridSelected = "";
        String gridSolved = "";
        StringBuilder sb = new StringBuilder();
        sb.append("\nTwoje rozwiązanie:\t\tWybrany diagram:\n");

        for (int i = 0; i < NUMBER_OF_CELLS; i++) {
            for (int j = 0; j < NUMBER_OF_CELLS; j++) {
                if (sudokuGrid[i][j] != 0)
                    gridSelected += sudokuGrid[i][j] + " ";
                else gridSelected += "  ";

                if (selectedGrid[i][j] != 0)
                    gridSolved += selectedGrid[i][j] + " ";
                else gridSolved += "  ";

            }
            sb.append(gridSelected).append("\t| \t").append(gridSolved).append("\n");
            gridSelected = "";
            gridSolved = "";
        }

        System.out.println(sb);
    }

    void prepareToResolve() {
        if (isPrepared) {
            copy(selectedGrid, sudokuGrid);
            for (int i = 0; i < NUMBER_OF_CELLS; i++) {
                for (int j = 0; j < NUMBER_OF_CELLS; j++) {
                    boolean schouldRemove = random.nextBoolean();
                    if (schouldRemove) {
                        sudokuGrid[i][j] = 0;
                    }
                }
            }
            printWithoutTime();
        } else System.err.println("Nie wybrano diagramu.");
    }

    void resolvingSudoku(int row, int column, int value) {
        sudokuGrid[column - 1][row - 1] = value;
        pushOnStack(column - 1, row - 1);
    }

    void readGridFromFile(String str) {
        isPrepared = true;
        int counter = 0;
        for (int i = 0; i < NUMBER_OF_CELLS; i++) {
            for (int j = 0; j < NUMBER_OF_CELLS; j++) {
                sudokuGrid[i][j] = Integer.valueOf(str.substring(counter, counter + 1));
                counter++;
            }
        }
    }

    void createSudokuAllGrid() {
        isPrepared = true;
        long startTime = System.currentTimeMillis();
        while (!duplicateInSquare) {
            for (int a = 0; a < sudokuGrid.length; a++) {
                for (int b = 0; b < sudokuGrid.length; b++) {
                    sudokuGrid[a][b] = 0;
                }
            }
            HashSet<Integer> arrayForCheckDuplicateInSquare = new HashSet<>();
            for (int i = 0; i < NUMBER_OF_CELLS; i++) {
                for (int j = 0; j < NUMBER_OF_CELLS; j++) {
                    boolean isDuplicate = false;
                    int randomNumber = random.nextInt(NUMBER_OF_CELLS) + 1;
                    int y = 0;
                    int z = 0;
                    if (j > 2) {
                        z = 3;
                    }
                    if (j > 5) {
                        z = 6;
                    }
                    if (i > 2) {
                        y = 3;
                    }
                    if (i > 5) {
                        y = 6;
                    }
                    for (int x = 0; x < NUMBER_OF_CELLS; x++, z++) {
                        if (sudokuGrid[y][z] == randomNumber || sudokuGrid[x][j] == randomNumber || sudokuGrid[i][x] == randomNumber) {
                            isDuplicate = true;
                            j--;
                            arrayForCheckDuplicateInSquare.add(randomNumber);
                            break;
                        }
                        if (z == 2 || z == 5 || z == 8) {
                            z -= 3;
                        }
                        if ((x + 1) % 3 == 0) {
                            y++;
                        }
                    }
                    if (!isDuplicate) {
                        sudokuGrid[i][j] = randomNumber;
                        arrayForCheckDuplicateInSquare.clear();
                    }
                    for (int a = 0; a < sudokuGrid.length; a++) {
                        for (int b = 0; b < sudokuGrid.length; b++) {
                            System.out.print(" " + sudokuGrid[a][b]);
                        }
                        System.out.println();
                    }
                    countingDrawnNumbers++;
                    System.out.print("--------------------Counting drawn numbers:" + countingDrawnNumbers + ", random number: " + randomNumber + ", coordination i:" + i + " j:" + j);
                    System.out.print(", array with duplicated numbers in square: ");
                    for (Integer integer : arrayForCheckDuplicateInSquare) {
                        System.out.print(integer + ", ");
                    }
                    System.out.println("array size: " + arrayForCheckDuplicateInSquare.size());
                    System.out.println();
                    if (arrayForCheckDuplicateInSquare.size() == 9) {
                        duplicateInSquare = true;
                        break;
                    }
                }
                if (duplicateInSquare) {
                    break;
                }
            }
            if (sudokuGrid[8][8] == 0) {
                duplicateInSquare = false;

            } else {
                duplicateInSquare = true;
                sudokuCreateTime = (System.currentTimeMillis() - startTime) / 1000f;
            }
        }
        duplicateInSquare = false;
    }


}

