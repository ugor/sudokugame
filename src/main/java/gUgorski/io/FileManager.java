package gUgorski.io;

import java.io.*;
import java.util.Random;
import java.util.Scanner;

public class FileManager {

    private static final int NUMBER_OF_CELLS = 9;
    private static final int FIRST_GRID_ROW_IN_FILE = 2;
    private static final int NEXT_SUDOKU_FIRST_GRID_ROW_IN_FILE = 14;
    private static final int FIRST_SUDOKU_NUMBER_IN_ROW = 4;


    public String readGridFromFile() throws FileNotFoundException {
        final String fileName = "allSudokuGrids.txt";
        File file = new File(fileName);
        Scanner scanner = new Scanner(file);

        int lines = 0;
        while (scanner.hasNextLine()) {
            scanner.nextLine();
            lines++;
        }
        System.out.println();
        System.out.println("Ilość wierszy w pliku: " + lines);
        System.out.println();

        Random random = new Random();
        int temp;
        String numbersForGrid = "";
        do {
            temp = random.nextInt(lines) + 1;

        } while ((temp - FIRST_GRID_ROW_IN_FILE) % (NEXT_SUDOKU_FIRST_GRID_ROW_IN_FILE - FIRST_GRID_ROW_IN_FILE) != 0);

        System.out.println("Diagram wylosowany z wiersza: " + temp);
        System.out.println();

        lines = 0;

        scanner = new Scanner(file);

        while (scanner.hasNextLine()) {
            StringBuilder stringBuilder = new StringBuilder();

            String name = scanner.nextLine();

            if (lines == temp) {

                for (int i = 0; i < NUMBER_OF_CELLS; i++) {
                    stringBuilder.append(name.substring(FIRST_SUDOKU_NUMBER_IN_ROW));
                    name = scanner.nextLine();
                    lines++;
                }
                numbersForGrid = stringBuilder.toString().replaceAll(" ", "");
            }
            lines++;
        }
        return numbersForGrid;
    }

    public void saveGridToFile(String str) {

        String fileName = "allSudokuGrids.txt";

        try (
                FileWriter fileWriter = new FileWriter(fileName, true);
                BufferedWriter writer = new BufferedWriter(fileWriter)
        ) {
            writer.write(String.valueOf(str));
        } catch (IOException e) {
            System.err.println("Failure to save file:" + fileName);
        }
    }
}
